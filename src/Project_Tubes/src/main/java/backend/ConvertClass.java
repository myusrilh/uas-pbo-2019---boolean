package backend;

import com.aspose.pdf.DocSaveOptions;
import com.aspose.pdf.Document;
import com.aspose.pdf.SaveFormat;
//import static frontend.Home2.dir;
//import static frontend.Home2.doc;
//import static frontend.Home2.filename;
//import static frontend.Home2.filenamenoext;
//import static frontend.Home2.saveOptions;
//import static frontend.Home2.txttujuan;
import org.apache.commons.io.FilenameUtils;

public class ConvertClass {

    public static String dataDir = Utils.getSharedDataDir(ConvertClass.class) + "DocumentConversion/";
    public static String dir, filename, filenamenoext;
    public static Document doc;
    public static DocSaveOptions saveOptions;

    public ConvertClass(String direktori, String namaFile, String fileTanpaEkstensi) {
        dir = direktori;
        filename = namaFile;
        filenamenoext = fileTanpaEkstensi;
    }

    public ConvertClass(Document dokumen, DocSaveOptions opsiSimpan) {
        doc = dokumen;
        saveOptions = opsiSimpan;
    }

    public static void main(String[] args) {
        //savingToDoc();
        savingToDOCX();
        //usingTheDocSaveOptionsClass();
    }

//	public static void savingToDoc() {
//		// Open the source PDF document
//		Document pdfDocument = new Document(dataDir + "SampleDataTable.pdf");
//		// Save the file into Microsoft document format
//		pdfDocument.save(dataDir + "TableHeightIssue.doc", SaveFormat.Doc);
//	}
//
//	public static void savingToDOCX() {
//		// Load source PDF file
//		Document doc = new Document(dataDir + "input.pdf");
//		// Instantiate Doc SaveOptions instance
//		DocSaveOptions saveOptions = new DocSaveOptions();
//		// Set output file format as DOCX
//		saveOptions.setFormat(DocSaveOptions.DocFormat.DocX);
//		// Save resultant DOCX file
//		doc.save(dataDir + "resultant.docx", saveOptions);
//	}
//
//	public static void usingTheDocSaveOptionsClass() {
//		// Open a document
//		// Path of input PDF document
//		String filePath = dataDir + "source.pdf";
//		// Instantiate the Document object
//		Document document = new Document(filePath);
//		// Create DocSaveOptions object
//		DocSaveOptions saveOption = new DocSaveOptions();
//		// Set the recognition mode as Flow
//		saveOption.setMode(DocSaveOptions.RecognitionMode.Flow);
//		// Set the Horizontal proximity as 2.5
//		saveOption.setRelativeHorizontalProximity(2.5f);
//		// Enable the value to recognize bullets during conversion process
//		saveOption.setRecognizeBullets(true);
//		// Save the resultant DOC file
//		document.save(dataDir + "Resultant.doc", saveOption);
//	}
    public static void savingToDoc() {
        // Open the source PDF document
//        Document pdfDocument = new Document(dataDir + "SampleDataTable.pdf");
        Document pdfDocument = new Document(dir);
        // Save the file into Microsoft document format
//        pdfDocument.save(dataDir + "TableHeightIssue.doc", SaveFormat.Doc);
        pdfDocument.save(dataDir + "TableHeightIssue.doc", SaveFormat.Doc);
    }

    public static void savingToDOCX() {
        // Load source PDF file
//        Document doc = new Document(dataDir + "input.pdf");
//        Document doc = new Document(dir);
        doc = new Document(dir);
        // Instantiate Doc SaveOptions instance
//      DocSaveOptions saveOptions = new DocSaveOptions();
        saveOptions = new DocSaveOptions();
        // Set output file format as DOCX
        saveOptions.setFormat(DocSaveOptions.DocFormat.DocX);
        // Save resultant DOCX file
//        doc.save(dataDir + "resultant.docx", saveOptions);
//        doc.save(dataDir + "hasil_convert.docx", saveOptions);

        filenamenoext = FilenameUtils.removeExtension(filename);
//        doc.save(dataDir + filenamenoext + ".docx", saveOptions);
        doc.save(dataDir + filenamenoext + ".docx", saveOptions);

    }

    public static void usingTheDocSaveOptionsClass() {
        // Open a document
        // Path of input PDF document
        //String filePath = dataDir + "source.pdf";
        String filePath = dir;
        // Instantiate the Document object
        Document document = new Document(filePath);
        // Create DocSaveOptions object
        DocSaveOptions saveOption = new DocSaveOptions();
        // Set the recognition mode as Flow
        saveOption.setMode(DocSaveOptions.RecognitionMode.Flow);
        // Set the Horizontal proximity as 2.5
        saveOption.setRelativeHorizontalProximity(2.5f);
        // Save format to DOCX
        saveOption.setFormat(DocSaveOptions.DocFormat.DocX);
        // Enable the value to recognize bullets during conversion process
        saveOption.setRecognizeBullets(true);
        filenamenoext = FilenameUtils.removeExtension(filename);
        String namaFile = filenamenoext + ".docx";
        // Save the resultant DOCX file
        document.save(dataDir + namaFile, saveOption);
        //document.save(dataDir + "Resultant.doc", saveOption);
    }

}
