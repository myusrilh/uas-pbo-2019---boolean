/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author asus
 */
public class Koneksi {

    public static Connection koneksi;
    public static String localhost, username, password, database;

    public static void buka_koneksi() {
        if (koneksi == null) {
            try {
                localhost = "jdbc:mysql://localhost:3306/" + database;
                username = "user";
                password = "";
                database = "log_history";
                DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
                koneksi = DriverManager.getConnection(localhost, username, password);

            } catch (SQLException e) {
                System.out.println("Error Koneksi!");
                e.printStackTrace();
            }
        }
    }
    
    
}
