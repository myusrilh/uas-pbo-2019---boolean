package ocr4 coba.ttu.ocr.teaching;

public class OCRTeachingException extends Exception {

	public OCRTeachingException(Exception e) {
		super(e);
	}

}
