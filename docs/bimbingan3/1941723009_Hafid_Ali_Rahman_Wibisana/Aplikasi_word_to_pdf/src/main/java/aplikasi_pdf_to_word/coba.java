/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi_pdf_to_word;

/**
 *
 * @author Hafid
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class coba {
    public static void main(String[] args) throws Exception {
          String inputFile="D:/TEST.docx";
          String outputFile="D:/TEST.pdf";
          if (args != null && args.length == 2) {
            inputFile=args[0];
            outputFile=args[1];
          }
          System.out.println("inputFile:" + inputFile + ",outputFile:"+ outputFile);
          FileInputStream in=new FileInputStream(inputFile);
          XWPFDocument document=new XWPFDocument(in);
          File outFile=new File(outputFile);
          OutputStream out=new FileOutputStream(outFile);
          PdfOptions options=null;
          PdfConverter.getInstance().convert(document,out,options);
        }
}
//public static void main( String[] args )
//{
//    long startTime = System.currentTimeMillis();
//
//    try
//    {
//        // 1) Load docx with POI XWPFDocument
//        XWPFDocument document = new XWPFDocument( Data.class.getResourceAsStream( "DocxBig.docx" ) );
//
//        // 2) Convert POI XWPFDocument 2 PDF with iText
//        File outFile = new File( "target/DocxBig.pdf" );
//        outFile.getParentFile().mkdirs();
//
//        OutputStream out = new FileOutputStream( outFile );
//        PdfOptions options = PdfOptions.create().fontEncoding( "windows-1250" );
//        PdfConverter.getInstance().convert( document, out, options );
//    }
//    catch ( Throwable e )
//    {
//        e.printStackTrace();
//    }
//
//    System.out.println( "Generate DocxBig.pdf with " + ( System.currentTimeMillis() - startTime ) + " ms." );
//}
//}
// 
