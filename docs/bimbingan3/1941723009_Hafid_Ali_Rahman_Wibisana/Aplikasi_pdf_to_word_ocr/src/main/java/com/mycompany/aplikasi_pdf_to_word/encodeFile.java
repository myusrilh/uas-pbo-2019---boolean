/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aplikasi_pdf_to_word;

/**
 *
 * @author Hafid
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.util.List;
import javax.imageio.ImageIO;

import org.ghost4j.document.DocumentException;
import org.ghost4j.document.PDFDocument;
import org.ghost4j.analyzer.FontAnalyzer;
import org.ghost4j.renderer.RendererException;
import org.ghost4j.renderer.SimpleRenderer;
import net.sourceforge.tess4j.*;

class encoder {
    public static byte[] createByteArray(File pCurrentFolder, String pNameOfBinaryFile) {
        String pathToBinaryData = pCurrentFolder.getAbsolutePath()+"/"+pNameOfBinaryFile;

        File file = new File(pathToBinaryData);
        if (!file.exists()) {
            System.out.println(pNameOfBinaryFile+" could not be found in folder "+pCurrentFolder.getName());
            return null;
        }

        FileInputStream fin = null;
        try {
            fin = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte fileContent[] = new byte[(int) file.length()];
        try {
            if (fin != null)
                fin.read(fileContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileContent;
    }

    public void covertToImage(File pdfDoc) {
        PDFDocument document = new PDFDocument();
        try {
            document.load(pdfDoc);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SimpleRenderer renderer = new SimpleRenderer();
        renderer.setResolution(300);
        List<Image> images = null;
        try {
            images = renderer.render(document);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RendererException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        try {
            if (images != null) {
                // for testing only 1 page
                ImageIO.write((RenderedImage) images.get(10), "png", new File("/home/cloudera/Downloads/1.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class encodeFile {
    public static void main(String[] args) throws TesseractException {
        /* This part is for pure PDF files i.e. not scanned */
//        byte[] arr = encoder.createByteArray(new File("D:/hafid/"), "test.pdf");
//        String result = javax.xml.bind.DatatypeConverter.printBase64Binary(arr);
//        System.out.println(result);

        /* This part create the image for a page of scanned PDF file */
        new encoder().covertToImage(new File("D:/TEST.pdf")); // results in 1.png

        /* This part is for OCR */
        Tesseract instance = new Tesseract();
        String res = instance.doOCR(new File("D:/hafid"));
        System.out.println(res);
    }
}
  

