/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend;

import com.aspose.pdf.DocSaveOptions;
import com.aspose.pdf.Document;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileSystemView;





/**
 *
 * @author MADHA
 */
public class Save {
    public static Document doc;
    private static String dataDir = Utils.getSharedDataDir(ConvertPDFToDOCOrDOCXFormat.class) + "DocumentConversion/";
     public static String filenamenoext;
     public static DocSaveOptions saveOptions;
     
    public static void simpan(){
    

            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            jfc.setDialogTitle("Choose a directory to save your file: ");
            jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int returnValue = jfc.showSaveDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                if (jfc.getSelectedFile().isDirectory()) {
                    dataDir = jfc.getSelectedFile().toString();
                    System.out.println("You selected the directory: " + jfc.getSelectedFile());
                    doc.save(dataDir + filenamenoext + ".docx", saveOptions);
                }
            }
       
    }
}
