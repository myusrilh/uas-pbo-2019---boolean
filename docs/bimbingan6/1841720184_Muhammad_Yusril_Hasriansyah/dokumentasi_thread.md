# Jobdesk Yusril

Masih bingung mengimplementasikan thread ke project.
Saya hanya menemukan dokumentasi tentang thread dan juga cara meng*interrupt*nya

- ![Starting And Defining Thread](https://docs.oracle.com/javase/tutorial/essential/concurrency/runthread.html)
- ![Interrupts](https://docs.oracle.com/javase/tutorial/essential/concurrency/interrupt.html)
- ![Pausing Execution](https://docs.oracle.com/javase/tutorial/essential/concurrency/sleep.html)