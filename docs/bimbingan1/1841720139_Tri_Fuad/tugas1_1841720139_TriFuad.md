## Jurnal Internasional

### Abstract
#### Indoneisa Version
  
  Tahap utama terakhir dalam sistem pengenalan plat nomor otomatis (ANPR) adalah pengenalan karakter optik (OCR), di mana karakter plat nomor pada gambar plat nomor dikonversi menjadi teks yang disandikan. Dalam penelitian ini, disajikan algoritme OCR berbasis jaringan syaraf tiruan untuk aplikasi ANPR dan arsitektur efisiennya. Arsitektur yang diusulkan telah berhasil diimplementasikan dan diuji dengan menggunakan papan pengembangan diprogram bidang grafis Mentor Graphics RC240 array (FPGA) yang dilengkapi dengan 4M Gates Xilinx Virtex-4 LX40. Database 3570 gambar karakter biner Inggris telah digunakan untuk menguji kinerja arsitektur yang diusulkan. Hasil yang dicapai telah menunjukkan bahwa arsitektur yang diusulkan dapat memenuhi persyaratan real-time dari sistem ANPR dan dapat memproses gambar karakter dalam 0,7 ms dengan tingkat pengenalan karakter yang berhasil 97,3% dan hanya mengkonsumsi 23% dari area yang tersedia dalam FPGA yang digunakan.

##### English Version
  Abstract: The last main stage in an automatic number plate recognition system (ANPRs) is optical character recognition (OCR), where the number plate characters on the number plate image are converted into encoded texts. In this study, an artiﬁcial neural network-based OCR algorithm for ANPR application and its efﬁcient architecture are presented. The proposed architecture has been successfully implemented and tested using the Mentor Graphics RC240 ﬁeld programmable gate arrays (FPGA) development board equipped with a 4M Gates Xilinx Virtex-4 LX40. A database of 3570 UK binary character images have been used for testing the performance of the proposed architecture. Results achieved have shown that the  proposed  architecture can meet the real-time requirement of an ANPR system and can process a character image in 0.7 ms with 97.3% successful character recognition rate and consumes only 23% of the available area in the used FPGA.

### Input
Foto dari plat nomor kendaraan 

### Process
kamera menangkap gambar plat kendaraan bermotor lalu degan proses yang cukup panjang dan pengelolaan komputerisasi dengan teknologi optical character recognition(OCR) lalu diubah dari yang semlua gambar plat nomor diubah menjadi text nomor kendaraan .

### Output
text dari nomor kendaraan 


## Jurnal Nasional

### Abstract
Safinah An-Naja adalah kitab fiqih dasar yang banyak dikaji di pesantren-pesantren salafiyyah. Di pondok pesantren Hidayatul Islamiyah Karawang, kitab ini sudah mulai di-digitalisasi dengan cara menyalin kitab tersebut ke dalam word-processor dan disimpan kedalam arsip. Kesulitannya adalah proses penginputan yang lama dan penulisan terjemahannya disimpan pada arsip terpisah. Pada penelitian ini akan dibuat sebuah aplikasi berbasis web yang dapat mengelola ayat-ayat beserta terjemahan kitab safinah an-naja sehingga pengguna dapat membaca dan mencari ayat dan terjemahan dengan mudah. Untuk mempermudah proses penginputan ayat ke dalam aplikasi digunakan software ReadIRIS yang mengimplementasikan metode OCR (optical character recognition) sehingga pengelola dapat dengan mudah memindai berkas agar langsung dapat dimasukkan ke dalam aplikasi. Aplikasi ReadIRIS ini nantinya juga diuji agar diketahui kondisi ideal pada saat memindai berkas kitab agar hasilnya optimal. 
 
Kata Kunci : Optical Character Recognition, Aplikasi Web, ReadIRIS 
### Input
gambar atau foto dari kitab yang bertulisakn arab
### Process
a. File Input File input berupa file citra digital dengan format *.bmp atau *.jpg atau *.TIFF 

b. Preprocessing Preprocessing merupakan suatu proses untuk menghilangkan bagian – bagian yang tidak diperlukan pada gambar input untuk proses selanjutnya. 

c. Segmentasi Segmentasi adalah proses memisahkan area pengamatan (region) pada tiap karakter yang dideteksi. 

d. Normalisasi Normalisasi adalah proses merubah dimensi region tiap karakter dan ketebalan karakter. 

e. Ekstraksi ciri Ekstraksi ciri adalah proses untuk mengambil ciri – ciri tertentu dari karakter yang diamati. 

f. Recognition Recognition merupakan proses untuk mengenali karakter yang diamati dengan cara membandingkan ciri – ciri karakter yang ada di dalam database. 

### Output
Format digital dari kitab yang bertulisakan arab