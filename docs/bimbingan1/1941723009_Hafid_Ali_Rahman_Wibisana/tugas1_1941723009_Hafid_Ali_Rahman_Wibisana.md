# Rangkuman Jurnal Tugas 1 (Tugas Besar PBO)

## Abstract
Document recognition and retrieval technologies
complement one another, providing improved access to
increasingly large document collections. While recognition
and retrieval of textual information is fairly mature, with
wide-spread availability of optical character recognition and
text-based search engines, recognition and retrieval of graphics such as images, figures, tables, diagrams, and mathematical expressions are in comparatively early stages of research.
This paper surveys the state of the art in recognition and
retrieval of mathematical expressions, organized around four
key problems in math retrieval (query construction, normalization, indexing, and relevance feedback), and four key problems in math recognition (detecting expressions, detecting
and classifying symbols, analyzing symbol layout, and constructing a representation of meaning). Of special interest
is the machine learning problem of jointly optimizing the
component algorithms in a math recognition system, and
developing effective indexing, retrieval and relevance feedback algorithms for math retrieval. Another important open
problem is developing user interfaces that seamlessly integrate recognition and retrieval. Activity in these important
research areas is increasing, in part because math notation
provides an excellent domain for studying problems common
to many document and graphics recognition and retrieval
applications, and also because mature applications will likely provide substantial benefits for education, research, and
mathematical literacy.
Keywords Math recognition · Graphics recognition ·
Mathematical information retrieval · Content-based image
retrieval · Human-computer interaction
OCR (Optical Character Recognition) adalah solusi efektif untuk proses konversi dokumen cetak menjadi dokumen digital. Permasalahan yang muncul dalam proses pengenalan huruf komputer adalah bagaimana teknik pengenalan mengidentifikasi berbagai jenis karakter dengan ukuran dan bentuk yang berbeda. Metode rekognisi yang digunakan dalam tugas akhir ini adalah metode korelasi pencocokan templat. Sebelum proses pengenalan, input gambar dengan format * .bmp atau * .jpg diproses terlebih dahulu pada proses preprocessing, yang meliputi binerisasi, segmentasi, dan normalisasi gambar. Tingkat keberhasilan pengakuan rata-rata sebesar 92,90% dihasilkan oleh sistem ini. Hasil akhir menunjukkan bahwa penggunaan metode korelasi pencocokan templat cukup efektif untuk membangun sistem OCR dengan akurasi yang baik.
## Input
File input berupa file citra digital dengan format *.bmp atau *.jpg.
## Proses
Adapun langkah-langkah yang harus ditempuh adalah sebagai berikut:
a. Menyiapkan citra uji untuk pengujian pertama dengan ketentuan sebagai berikut:
1.	Citra uji terdiri dari 5 buah citra dengan jenis huruf yang berbeda, yaitu Arial, Times New Roman, Comic Sans MS, Cambria, dan Courier New. Untuk lebih jelasnya bisa dilihat pada gambar
4.10 sampai 4.15.
2.	Citra uji berisi sebuah paragraf yang terdiri dari 263 karakter (tidak termasuk tanda baca dan spasi).
3.	Ukuran huruf yang digunakan adalah 12pt dan style huruf yang digunakan adalah normal.
## Output
Data hasil pengenalan aplikasi OCR pada pengujian pertama (menggunakan huruf ukuran 12pt) dapat dilihat pada tabel 4.1.
Data hasil pengenalan aplikasi OCR pada pengujian kedua (menggunakan huruf ukuran 20pt) dapat dilihat pada tabel 4.2.
