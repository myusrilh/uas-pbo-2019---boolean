# Rangkuman Jurnal Tugas 1 (Tugas Besar PBO)

## Abstract

    (Nasional)
    Penelitian ini bertujuan untuk mengembangkan model pengarsipan digital dengan system file untuk diaplikasikan pada pengarsipan di Universitas Terbuka, pengarsipan dengan system file ini akan menggantikan system file manual dan pengrasipan digital yang berbasis database (BLOB), dan adapun tahapan pekerjaan sebagai berikut :
    1. Melakukan konversi BLOB ke file JPG.
    2. Melakukan konversi BLOB ke file BITMAP dan kemudian membentuknya menjadi file PDF (Portable Document Feeder).
    3. Memanejemen penyimpanan system pengarsipan file dengan menggunakan Basis Data (Database).
    4. Membuat INDEX/Pencarian data Arsip dengan menggunakan NIM.
    Desain Model Pengarsipan Digital dengan Sistem File disarankan untuk digunakan bukan hanya pada pengarsipan dokumen mahasiswa akan tetapi lebih meluas kepada dokumen-dokumen yang lain.
    Kata kunci: Inde, Database, Portable, Feeder, document

    (international)
    Optical Character Recognition (OCR) is a technique, used to convert scanned image into editable text format. Many different types of Optical Character Recognition (OCR) tools are commercially available today; it is a useful and popular method for different types of applications. OCR can predict the accurate result depends on text pre-processing and segmentation algorithms. Image quality is one of the most important factors that improve quality of recognition in performing OCR tools. Images can be processed independently (.png, .jpg, and .gif files) or in multi-page PDF documents (.pdf). The primary objective of this work is to provide the overview of various Optical Character Recognition (OCR) tools and analyses of their performance by applying the two factors of OCR tool performance i.e. accuracy and error rate.


## Input
    Input image is digitalized images like a scanned or captured text image. It may be of different formats, i.e. JPG, PNG, BMP, GIF, TIFF and multi-page PDF files.

## Proses
    Conversion
    Mengkonversi dokumen adalah proses mengubah dokumen word processor atau spreadsheet menjadi data gambar permanen untuk disimpan pada sistem komputerisasi.

## Output

    The result of the input images is displayed in the output text
    

    