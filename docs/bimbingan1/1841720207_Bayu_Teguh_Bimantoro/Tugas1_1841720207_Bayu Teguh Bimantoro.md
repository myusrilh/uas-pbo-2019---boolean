# Rangkuman Jurnal Tugas 1 (Tugas Besar PBO)

## Abstract
   Safinah An-Naja adalah kitab fiqih dasar yang banyak dikaji di pesantren-pesantren salafiyyah. Di pondok pesantren Hidayatul Islamiyah Karawang,  kitab ini sudah mulai di-digitalisasi dengan cara menyalin kitab tersebut ke dalam word-processor dan disimpan kedalam arsip. Kesulitannya adalah proses penginputan yang lama dan penulisan terjemahannya disimpan pada arsip terpisah. Pada penelitian ini akan dibuat sebuah aplikasi berbasis web yang dapat mengelola ayat-ayat beserta terjemahan kitab safinah an-naja sehingga pengguna dapat membaca dan mencari ayat dan terjemahan dengan mudah. Untuk mempermudah proses penginputan ayat ke dalam aplikasi digunakan software ReadIRIS yang mengimplementasikan metode OCR (optical character recognition) sehingga pengelola dapat dengan mudah memindai berkas agar langsung dapat dimasukkan ke dalam aplikasi. Aplikasi ReadIRIS ini nantinya juga diuji agar diketahui kondisi ideal pada saat memindai berkas kitab agar hasilnya optimal. 
 
## Input
   File input berupa file citra digital dengan format *.bmp atau *.jpg atau *.TIFF 

## Proses
### ReadIRIS

ReadIRIS adalah sebuah software OCR (Optical Character Recognition) yang dirancang untuk meningkatkan kemampuan piranti scanner agar dapat mengenali dan mengkonversi berkas, gambar, atau dokumen PDF agar dapat diubah dan dikelola kedalam file digital (word, excel, PDF, HTML, dll). ReadIRIS memungkinkan kita untuk membuat file PDF mudah dicari dan diindex agar kompatibel untuk semua komputer. Berikut adalah beberapa fitur yang dimiliki oleh ReadIRIS berdasarkan situsnya di http://www.irislink.com

Pada penelitian ini, software OCR ReadIRIS dipilih setelah dilakukan perbandingan dengan dua software OCR yang lain yaitu Omnipage dan ABBYY FineReader. Pengujian terhadap ReadIRIS dilakukan untuk mengetahui tingkat akurasi dari konversi huruf arab yang dilakukan oleh ReadIRIS, juga untuk mendapatkan teknik pemindaian yang otimal. Pengujian dilakukan dengan beberapa parameter, yaitu: 
1. Pengujian terhadap jenis huruf arab Jenis huruf arab yang dimaksud adalah huruf arab yang berharakat dan duruf arab yang tidak berharakat.  
2. Pengujian hasil file pemindaian Ada tiga macam pengujian yang akan dilakukan terhadap file hasil pemindaian. Pertama pengujian dengan mambandingkan resolusi, yaitu resolusi dengan 200dpi , 300dpi dan 600dpi. Kedua, membandingkan dua jenis scanner yaitu tipe scanner Scanjet dengan tipe scanner all in one. Ketiga, akan dilakukan pengujian akurasi terhadap file yang dipotret dengan kamer SLR. 

Pengujian ReadIRIS dilakukan dengan menggunakan dokumen kitab fiqih safinah an-naja tanpa harakat pada halaman ke-16 yang mengandung ± 3000 huruf. Selanjutnya dilakukan pengujian untuk menentukan tipe scanner yang paling tepat untuk digunakan pada saat memindai dokumen. Tipe scanner dibagi menjadi dua kategori yaitu scanner multifungsi (biasanya menyatu bersama printer) dan scanner biasa 

## Output
Aplikasi terjemahan kitab fiqih safinah an-naja

## Link web converter pdf to word
https://pdf2doc.com/id/