# Rangkuman Jurnal Tugas 1 (Tugas Besar PBO)

## Abstract
Dokumen ini menjelaskan tentang teknologi _Optical Character Recognition_ atau OCR. Dimana OCR sendiri merupakan pemindaian karakter berdasarkan tingkat keabu-abuan atau hitam-putih dalam sebuah teks/dokumen, kemudian dokumen tersebut diubah menjadi bentuk vektor dimana user bisa memanipulasi isi dari teks tersebut nantinya.
Tentu saja hal ini berguna untuk zaman sekarang yang serba digital. Agar manusia tidak perlu bekerja dua kali, maka bisa dilakukan perubahan format teks, sehingga manusia bisa melakukan _editing_ sebelum mencetak dokumen tersebut. Teknologi ini diterapkan pada aplikasi **_"Converter PDF (berisi Math Equation) to Word"_**

## Input
File PDF berisi perhitungan matematika, user _drag and drop_ atau upload dokumen PDF kepada aplikasi.

## Proses
Dilakukan _scanning_ pada file yang terupload. _scanning_ menggunakan teknologi OCR, semua data pada dokumen dijadikan grayscale (hitam-putih) terlebih dahulu

## Output
Dokumen yang sudah terkonversi menjadi dokumen vektor yang sebelumnya 
