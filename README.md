# UAS PBO 2019 - Boolean

## Aplikasi : Converter PDF(Math Equation) to WORD dengan teknologi OCR :)

### Mata Kuliah = Pemrograman Berbasis Objek

### Anggota Kelompok Boolean (TI-2C)
1. Ahmad Muzakhy (02 - 1841720116)
2. Bayu Teguh Bimantoro (05 - 1841720207)
3. Hafid Ali Rahman Wibisana (13 - 1941723009)
4. Muhammad Yusril Hasriansyah (24 - 1841720184)
5. Tri Fuad (26 - 1841720139)

### Cara Penggunaan Aplikasi
1. Pertama siapkan dokumen *.pdf yang ingin diubah menjadi *.docx
2. Kemudian jalankan aplikasi "pdf to word converter (math equation)"
3. Pada saat aplikasi sudah terbuka, maka tekan tombol upload
4. Setelah itu cari lokasi tempat dokumen anda berada dan pilih dokumen tersebut
5. Terakhir tekan tombol convert dan tunggu proses hingga selesai
6. Setelah itu tekan save file untuk menyimpan aplikasi.
7. Dokumen yang terkonversi pun sudah bisa digunakan

# Note : Dokumen yang terkonversi telah otomatis tersimpan di Folder "Dokumen Konversion"